// Package auth defines Authentication and Authorization interfaces which are used in
// to authenticate and authorized endpoints.
package auth

import (
	"errors"
)

// Authenticator authorizes access to an endpoint. If error is nil, then
// the request is authorized. The error should contain useful information
// which identifies why the authorization failed
type Authenticator interface {
	Authenticate(interface{}) error
	Authorizor() Authorizor
}

// Authorizor authorizes access to an endpoint. If error is nil, then
// the request is authorized. The error should contain useful information
// which identifies why the authorization failed
type Authorizor interface {
	Authorize(interface{}) error
}

// AllowAll allows all requests
type AllowAll struct{}

// Authorize always allows a request
func (auth *AllowAll) Authorize(interface{}) error {
	return nil
}

// Authenticate always allows a request
func (auth *AllowAll) Authenticate(interface{}) error {
	return nil
}

// DenyAll denies all requests.
type DenyAll struct{}

// Authenticate always denies a request
func (auth *DenyAll) Authenticate(interface{}) error {
	return errors.New("DenyAll")
}

// Authorize always denies a request
func (auth *DenyAll) Authorize(interface{}) error {
	return errors.New("DenyAll")
}
