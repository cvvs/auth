package auth_test

import (
	"errors"
	"fmt"
	"net/http"
	"testing"

	"bitbucket.org/cvvs/auth"
)

type basicAuth struct {
	Users map[string]string
}

func (ba *basicAuth) Authenticate(input interface{}) error {
	req, ok := input.(*http.Request)
	if !ok {
		return errors.New("basic auth: parameter is not an http request")
	}
	username, password, ok := req.BasicAuth()
	if !ok {
		return errors.New("basic auth: not supported")
	}
	// A real password db would have scrypt hashed, not plaintext passwords.
	stored, ok := ba.Users[username]
	if !ok {
		return errors.New("basic auth: no such user " + username)
	} else if password == stored {
		return nil
	}
	return errors.New("basic auth: password mismatch")
}

func (ba *basicAuth) Authorizor() auth.Authorizor {
	return new(auth.AllowAll)
}

func ExampleAuthenticator() {
	var auth auth.Authenticator = &basicAuth{Users: map[string]string{"luggage": "12345"}}

	req, _ := http.NewRequest(http.MethodGet, "http://www.example.com", nil)
	req.SetBasicAuth("luggage", "12345")

	if err := auth.Authenticate(req); err != nil {
		fmt.Println("error authenticating user: ", err)
	}

	if err := auth.Authorizor().Authorize(req); err != nil {
		fmt.Println("error authorizing user")
	}
}

func TestDenyAllAllowAll(t *testing.T) {
	deny := new(auth.DenyAll)

	if err := deny.Authenticate(nil); err == nil {
		t.Error("deny should have returned and error")
	}

	allow := new(auth.AllowAll)

	if err := allow.Authenticate(nil); err != nil {
		t.Error("allow should not return an error")
	}
}
