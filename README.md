# auth

Define interfaces used for authentication and authorization

An non-compiling example of basic auth. Don't use this in the real world, it's just an example
of an implementation.

    ...

    type basicAuth struct {
        Users map[string][]byte
    }

    func (ba *basicAuth) Authenticate(input interface{}) error {
        req, ok := input.(*http.Request)
        if !ok {
            return errors.New("basic auth: parameter is not an http request")
        }
        username, password, ok := req.BasicAuth()
        if !ok {
            return errors.New("basic auth: not supported")
        }

        stored, ok := ba.Users[username]
        if !ok {
            return errors.New("basic auth: no such user " + username)
        } 

        return bcrypt.CompareHashAndPassword(stored, []byte(pasword))
    }

    func (ba *basicAuth) Authorizor() auth.Authorizor {
        return new(auth.AllowAll)
    }

    func newBasicAuthenticator() *basicAuth {
        // Don't ignore errors, especially in auth code
        hash, _ := bcrypt.GenerateFromPassword([]byte("12345"), 0)
        m := make(map[string][]byte)
        m["luggage"] = hash

        return &basicAuth{Users: m}
    }

    var auth auth.Authenticator = newBasicAuthenticator()

    func(rw http.ResponseWriter, req *http.Request){

	    if err := auth.Authenticate(req); err != nil {
	    	http.NewError(rw, "401 Unauthorized", http.StatusUnauthorized)
            return
	    }

        // This is an allow all authorizor so it will never fail.
	    if err := auth.Authorizor().Authorize(req); err != nil {
			http.NewError(rw, "401 Unauthorized", http.StatusForbidden)
            return
	    }
    }

    ...
